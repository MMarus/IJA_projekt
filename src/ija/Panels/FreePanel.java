/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.Panels;

import ija.UI.Images;
import ija.UI.TUI;
import static ija.ija.LOG;
import ija.treasure.TreasureCard;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author xjezov01
 */
public class FreePanel extends JPanel implements ActionListener{
    
    //private JLabel FREE;
    private JButton FREE;

    /**
     *
     */
    public FreePanel() {
        setLayout(null);
    }
    
    /**
     *
     */
    public void draw()
    {
        removeAll();
        
        setLayout(new GridLayout(1, 1, 0, 0));
        
        //FREE = new JLabel();
        FREE = new JButton();
        FREE.addActionListener(this);
        add(FREE);
        FREE.setMargin(new Insets(0,0,0,0));
        FREE.setLayout(new GridLayout(3, 3, 0, 0));
        JLabel ButtonLabels[][] = createLabels();
        

        // with FreeCard
        //ImageIcon icon = new ImageIcon(LOG.getIMGS().getMazeCardImg(LOG.showBoard().getFreeCard()));
        
        // with FreeField
        ImageIcon icon = new ImageIcon(LOG.getIMGS().getMazeCardImg(LOG.showBoard().getFreeField().getCard()));
        
        FREE.setIcon(new ImageIcon(icon.getImage()));
        
        TreasureCard tr = LOG.showBoard().getFreeField().getTreasure();
        if (tr != null) {
            Image item = LOG.getIMGS().getTreasureImg(tr);
            icon = new ImageIcon(Images.resize(item, 200/3,200/3 ));
            ButtonLabels[1][1].setIcon(icon);
        }
        
        for(int x = 0; x < 3; x++){
            for(int y = 0; y < 3; y++ ){
                FREE.add(ButtonLabels[x][y]);
            }
        }
    }
    
    //toto asi neni treba
    private JLabel[][] createLabels(){
        JLabel[][] labels = new JLabel[3][3];
        for ( int i = 0; i < 3; i++){
            for( int j = 0; j< 3; j++){
                labels[i][j]=new JLabel();
            }
        }
        return labels;
    }
    
    
    /**
     *
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        //Volame logiku s dotazom na otocenie freee kamenu
        /**
         * kliknutie na free kamen, button cislo 12
         */
        LOG.GameProcess(12,0,0);
        
        //stare
        //LOG.showBoard().getFreeField().getCard().turnRight();
        
        removeAll();
        LOG.OKNO.drawAll();
        
        
        
        
    }
    

}
