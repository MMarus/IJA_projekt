/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.Panels;


import static ija.ija.LOG;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author xjezov01
 */
public class StatsPanel extends JPanel  {
    
    private JLabel statDATA;

    /**
     *
     */
    public StatsPanel() {
        setLayout(null);

    }
    
    /**
     *
     */
    public void draw()
    {
        removeAll();
        
        setLayout(new GridLayout(1, 1, 0, 0));
        
        statDATA = new JLabel(LOG.showStats());
        add(statDATA);
        
        statDATA.setFont(new java.awt.Font("Tahoma", 0, 15));


    }

    
    
}
