/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.Panels;

import ija.board.*;
import ija.treasure.*;
import ija.UI.*;
import static ija.ija.LOG;

import java.awt.Insets;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import javax.swing.BorderFactory;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

/**
 * z
 *
 * @author marek
 */
public class BoardPanel extends JPanel implements ActionListener {

    private int size;
    private JButton button;

    int width, height;

    /**
     *
     */
    public BoardPanel() {
        size = 0;
        setLayout(null);
    }

    /**
     *
     */
    public void draw() {
        removeAll();
        size = LOG.showBoard().getN();
        Image image;

        TUI TxtUI = new TUI(LOG.showBoard());
        TxtUI.showBoard();
        setLayout(new GridLayout(size, size, 0, 0));

        Border onTurn = createBorderOnTurn();
        
        
        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= size; j++) {
                MazeField field = LOG.showBoard().get(i, j);
                Image img = Images.resize(LOG.getIMGS().getMazeCardImg(field.getCard()), 660/size, 660/size);

                button = new JButton();
                
                button.addActionListener(this);
                add(button);
                button.setIcon(new ImageIcon(img));
                button.setMargin(new Insets(0,0,0,0));
                button.setLayout(new GridLayout(3, 3, 0, 0));
                
                TreasureCard tr = field.getTreasure();
                
                
                JLabel ButtonLabels[][] = createLabels();//new JLabel[3][3];
                
                //Pridame treasure
                if (tr != null) {
                    Image item = LOG.getIMGS().getTreasureImg(tr);
                    ImageIcon icon = new ImageIcon(Images.resize(item, (660/size/3), (660/size/3)));
                    ButtonLabels[1][1].setIcon(icon);
                }
                //Pridame hracov
                
                for( int player = 0 ; player < LOG.getPlayers().getCount(); player++){

                    if( LOG.getOnePlayer(player).getX() == i && LOG.getOnePlayer(player).getY() == j){
                        Image item = LOG.getIMGS().getPlayerImg(player);
                        ImageIcon icon = new ImageIcon(Images.resize(item, (660/size/3), (660/size/3)));                    
                        if( ButtonLabels[0][0].getIcon() == null){
                            ButtonLabels[0][0].setIcon(icon);
                            if(LOG.getOnePlayer(player) == LOG.getPlayers().getPlayerOnTurn() ){
                                ButtonLabels[0][0].setBackground(Color.lightGray);
                                ButtonLabels[0][0].setOpaque(true);
                            }
                        }
                        else if( ButtonLabels[0][2].getIcon() == null){
                            ButtonLabels[0][2].setIcon(icon);
                            if(LOG.getOnePlayer(player) == LOG.getPlayers().getPlayerOnTurn() ){
                                ButtonLabels[0][2].setBackground(Color.lightGray);
                                ButtonLabels[0][2].setOpaque(true);
                            }
                        }
                        else if( ButtonLabels[2][0].getIcon() == null){
                            ButtonLabels[2][0].setIcon(icon);
                            if(LOG.getOnePlayer(player) == LOG.getPlayers().getPlayerOnTurn() ){
                                ButtonLabels[2][0].setBackground(Color.lightGray);
                                ButtonLabels[2][0].setOpaque(true);
                            }
                        }
                        else{
                            ButtonLabels[2][2].setIcon(icon);
                            if(LOG.getOnePlayer(player) == LOG.getPlayers().getPlayerOnTurn() ){
                                ButtonLabels[2][2].setBackground(Color.lightGray);
                                ButtonLabels[2][2].setOpaque(true);
                            }
                        }
                    }
                    
                }
                
                for(int x = 0; x < 3; x++){
                    for(int y = 0; y < 3; y++ ){
                        button.add(ButtonLabels[x][y]);
                    }
                }

            }
        }

    }
//toto asi neni treba
    private JLabel[][] createLabels(){
        JLabel[][] labels = new JLabel[3][3];
        for ( int i = 0; i < 3; i++){
            for( int j = 0; j< 3; j++){
                labels[i][j]=new JLabel();
            }
        }
        return labels;
    }
    
    private Border createBorderOnTurn(){
        Border greenBorder = BorderFactory.createLineBorder(Color.WHITE, 1);
        Border blueBorder = BorderFactory.createLineBorder(Color.YELLOW, 1);
        Border magentaBorder = BorderFactory.createLineBorder(Color.BLACK, 1);
        Border twoColorBorder = new CompoundBorder(magentaBorder, blueBorder);
        Border threeColorBorder = new CompoundBorder(twoColorBorder, greenBorder);
        return threeColorBorder;
    }
    
    /**
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Klik");

        JButton button = (JButton) e.getSource();
        Dimension size = button.getSize();

        System.out.println("size " + size);

        int buttonX = button.getX();
        int buttonY = button.getY();
        int buttonPosX = buttonX / size.width;
        int buttonPosY = buttonY / size.height;

        System.out.println("KLIK x = " + (buttonPosY+1) + "y= " + (buttonPosX+1) );
        
        /**
         * kliknutie na boardku, button cislo 13
         */
        LOG.GameProcess(13,buttonPosY + 1, buttonPosX + 1);

        //LOG.shift(buttonPosY + 1, buttonPosX + 1);

        LOG.OKNO.drawAll();

    }

}
