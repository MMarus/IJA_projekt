/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.Panels;

import static ija.ija.LOG;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author xjezov01
 */
public class MyCardPanel extends JPanel  implements ActionListener {
    
    
    private JButton button;

    /**
     *
     */
    public MyCardPanel() {
        setLayout(null);
        
    }
    
    /**
     *
     */
    public void draw()
    {
        removeAll();
        
        setLayout(new GridLayout(1, 1, 0, 0));
        
        button = new JButton();
        button.addActionListener(this);
        add(button);
        
        
        // tu CASE S 24 ROZNYMI KARTAMI + 1 ako GET CARD ON CLICK
        //ImageIcon icon = new ImageIcon(getClass().getResource("/resources/images/img.png"));
        
        ImageIcon icon = new ImageIcon(LOG.getIMGS().getTreasureImg( LOG.getPlayer().getMyCard() ));
        
        button.setIcon(new ImageIcon(icon.getImage()));
  
    }
    
    /**
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("KlikMYCARD");
        //LOG.cardClick();
        
        /**
         * kliknutie na kartu, button cislo 11
         */
        LOG.GameProcess(11,0,0);
        LOG.OKNO.drawAll();
        
    }
    
}
