package ija.board;

/**
 *
 * @author fjezo_000
 */
public class MazeCard {

    /**
     *
     */
    public CANGO left;

    /**
     *
     */
    public CANGO up;

    /**
     *
     */
    public CANGO right;

    /**
     *
     */
    public CANGO down;

    /**
     *
     */
    public static enum CANGO {

        /**
         *
         */
        LEFT,

        /**
         *
         */
        UP,

        /**
         *
         */
        RIGHT,

        /**
         *
         */
        DOWN
    }

    /**
     *
     * @param left
     * @param up
     * @param right
     * @param down
     */
    public MazeCard(CANGO left, CANGO up, CANGO right, CANGO down) {
        this.left = left;
        this.up = up;
        this.right = right;
        this.down = down;
    }

    /**
     *
     * @param type
     * @return
     */
    public static MazeCard create(String type) {
        MazeCard NEWW;

        switch (type) {
            case "L": // L rohova cesta -- zlava hore
                NEWW = new MazeCard(CANGO.LEFT, CANGO.UP, null, null);
                break;

            case "I": // ROVNA CESTA -- zlava doprava
                NEWW = new MazeCard(CANGO.LEFT, null, CANGO.RIGHT, null);
                break;

            case "T": // T cesta -- zlava hore aj doprava
                NEWW = new MazeCard(CANGO.LEFT, CANGO.UP, CANGO.RIGHT, null);
                break;

            default:
                throw new IllegalArgumentException();
        }

        return NEWW;

    }

    /**
     *
     */
    public void turnRight() {
        CANGO TEMP;

        TEMP = this.left;

        this.left = this.down;
        this.down = this.right;
        this.right = this.up;
        this.up = TEMP;
    }

    /**
     *
     * @param n
     */
    public void turn(int n) {
        int i;
        for (i = 0; i < n; i++) {
            turnRight();
        }
    }

    /**
     *
     * @param dir
     * @return
     */
    public boolean canGo(MazeCard.CANGO dir) {
        if (dir == CANGO.LEFT) {
            if (this.left != null) {
                return true;
            } else {
                return false;
            }
        } else if (dir == CANGO.UP) {
            if (this.up != null) {
                return true;
            } else {
                return false;
            }
        } else if (dir == CANGO.RIGHT) {
            if (this.right != null) {
                return true;
            } else {
                return false;
            }
        } else if (dir == CANGO.DOWN) {
            if (this.down != null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }
}
