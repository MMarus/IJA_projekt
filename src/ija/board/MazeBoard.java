package ija.board;

import ija.board.MazeCard.CANGO;
import static ija.ija.LOG;
import ija.treasure.CardPack;
import ija.treasure.TreasureCard;
import java.awt.Dimension;
import java.util.Random;

/**
 *
 * @author fjezo_000
 */
public class MazeBoard {

    //Kolko je aktualne Tciek, Liek Iciek
    int TS;
    int LS;
    int IS;
    //Max jedneho typu
    int MaxS;
    int n;

    MazeField BOARD[][];
    MazeField FREEFIELD = null;

    /**
     *
     * @param n
     */
    public MazeBoard(int n) {
        this.n = n;
        TS = 0;
        LS = 4;
        IS = 0;
        //Get max count of one type button, +1
        MaxS = n * n / 3 + 1;
        this.BOARD = new MazeField[n + 1][n + 1];
    }

    /**
     *
     * @param n
     * @return
     */
    public static MazeBoard createMazeBoard(int n) {
        MazeBoard nove = new MazeBoard(n);

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                nove.BOARD[i][j] = new MazeField();
            }
        }

        return nove;

    }

    /**
     *
     * @param items
     */
    public void newGame() {
        int turns = 0;
        // generujem a umiestnim kamene do policok
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                turns = 0;
                if ((i % 2) == 1 && (j % 2) == 1) {
                    this.BOARD[i][j].kamen = MazeCard.create("T");
                    if (i == 1) {
                        turns = 2;
                    } else if (j == 1) {
                        turns = 1;
                    } else if (j == n) {
                        turns = 3;
                    } else if (i == n) {
                        turns = 0;
                    } else {
                        turns = getIntRand(3);
                    }
                    this.BOARD[i][j].kamen.turn(turns);
                    TS += 1;
                } else {
                    //random generacia kamena, plus random otacanie
                    this.BOARD[i][j].kamen = MazeCard.create(myRAND());
                    this.BOARD[i][j].kamen.turn(getIntRand(3));
                }
            }
        }

        this.BOARD[1][1].kamen = MazeCard.create("L");
        this.BOARD[1][1].kamen.turn(2);

        this.BOARD[1][n].kamen = MazeCard.create("L");
        this.BOARD[1][n].kamen.turn(3);

        this.BOARD[n][1].kamen = MazeCard.create("L");
        this.BOARD[n][1].kamen.turnRight();

        this.BOARD[n][n].kamen = MazeCard.create("L");

        // vytvorime volny kamen
        MazeCard FREECARD = MazeCard.create(myRAND());
        FREECARD.turn(getIntRand(3));
        this.FREEFIELD = new MazeField();
        this.FREEFIELD.kamen = FREECARD;

        //vygeneruj treasurky
        generateTreasures();

    }

    private void generateTreasures() {
        int i, x, y;

        for (i = 0; i < LOG.getItemsPack().maxSize(); i++) {
            while (true) {
                x = getIntRand(n);
                x++;
                y = getIntRand(n);
                y++;
                if (this.BOARD[x][y].getTreasure() == null) {
                    this.BOARD[x][y].item = new TreasureCard(i);
                    break;
                }
            }
        }
    }

    /**
     *
     * @param r
     * @param c
     * @return
     */
    public MazeField get(int r, int c) {
        if (r > this.n || c > this.n) {
            return null;
        }

        return this.BOARD[r][c];
    }

    /**
     *
     * @return
     */
    public MazeField getFreeField() {
        return this.FREEFIELD;
    }

    /**
     *
     * @return
     */
    public String myRAND() {
        int mojRAND;
        String type;
        if (TS >= MaxS && LS >= MaxS && IS >= MaxS) {
            return "I";
        }

        Random randcislo = new Random();
        mojRAND = randcislo.nextInt(3); // nextInt robi rozmedzie random int cisel
        if (mojRAND == 0 && TS <= MaxS) {
            TS += 1;
            type = "T";
        } else if (mojRAND == 1 && LS <= MaxS) {
            LS += 1;
            type = "L";
        } else if (mojRAND == 2 && IS <= MaxS) {
            IS += 1;
            type = "I";
        } else {
            type = myRAND();
        }

        return type;
    }

    /**
     *
     * @param n
     * @return
     */
    public int getIntRand(int n) {
        Random randcislo = new Random();
        int num = randcislo.nextInt(n);
        return num;
    }

    /**
     *
     * @return
     */
    public int getN() {
        return n;
    }

    public boolean setField(int X, int Y, int item, int DOWN, int LEFT, int RIGHT, int UP){
        if( X > n || X < 0 || Y > n || Y < 0)
            return false;
        
        CANGO down = null;
        CANGO left = null;
        CANGO right = null;
        CANGO up = null;
        
        if( DOWN == 1)
            down = CANGO.DOWN;
        if( LEFT == 1)
            left = CANGO.LEFT;
        if( RIGHT == 1)
            right = CANGO.RIGHT;
        if( UP == 1)
            up = CANGO.UP;
  
        if( item == -1)
            BOARD[X][Y].createField(new MazeCard(left, up, right, down), null);
        else
            BOARD[X][Y].createField(new MazeCard(left, up, right, down), new TreasureCard(item));
        
        return true;
    }
    
    public boolean setFreeField(int item, int DOWN, int LEFT, int RIGHT, int UP){
        CANGO down = null;
        CANGO left = null;
        CANGO right = null;
        CANGO up = null;
        
        if( DOWN == 1)
            down = CANGO.DOWN;
        if( LEFT == 1)
            left = CANGO.LEFT;
        if( RIGHT == 1)
            right = CANGO.RIGHT;
        if( UP == 1)
            up = CANGO.UP;
  
        FREEFIELD = new MazeField();
        
        if( item == -1)
            FREEFIELD.createField(new MazeCard(left, up, right, down), null);
        else
            FREEFIELD.createField(new MazeCard(left, up, right, down), new TreasureCard(item));
        
        return true;
    }
    
    
    /**
     *
     * @param TMProw
     * @param TMPcol
     * @return Dimension
     */
    public Dimension shiftFIELD(int TMProw, int TMPcol) {

        MazeField TEMP;
        Dimension CannotGO;
        CannotGO = new Dimension();

        if (TMProw == 1 && TMPcol % 2 == 0) //kamene sa posunu DOLE v stlpci col
        {
            TEMP = this.BOARD[this.n][TMPcol]; // posledny

            for (int i = this.n; i > 1; i--) {
                // col sa nemeni

                this.BOARD[i][TMPcol] = this.BOARD[i - 1][TMPcol];
            }

            this.BOARD[TMProw][TMPcol] = this.FREEFIELD;

            this.FREEFIELD = TEMP;

            //System.out.println("POSUV>" + TMPcol);
            for (int k = 0; k < LOG.getPlayers().getCount(); k++) {
                if (LOG.getOnePlayer(k).getY() == TMPcol) {
                    int POSUV = LOG.getOnePlayer(k).getX() + 1;
                    if (POSUV > this.n) {
                        POSUV = 1;
                    }

                    LOG.getOnePlayer(k).setXY(POSUV, TMPcol);
                }
            }
            CannotGO.width = this.n;
            CannotGO.height = TMPcol;
            return CannotGO;
        }

        //kamene sa posunu HORE v stlpci col
        if (TMProw == this.n && TMPcol % 2 == 0) {
            TEMP = this.BOARD[1][TMPcol]; // posledny

            for (int i = 1; i < this.n; i++) {

                this.BOARD[i][TMPcol] = this.BOARD[i + 1][TMPcol];
            }

            this.BOARD[TMProw][TMPcol] = this.FREEFIELD;

            this.FREEFIELD = TEMP;

            for (int k = 0; k < LOG.getPlayers().getCount(); k++) {
                if (LOG.getOnePlayer(k).getY() == TMPcol) {
                    int POSUV = LOG.getOnePlayer(k).getX() - 1;
                    if (POSUV < 1) {
                        POSUV = this.n;
                    }

                    LOG.getOnePlayer(k).setXY(POSUV, TMPcol);
                }
            }
            
            CannotGO.width = 1;
            CannotGO.height = TMPcol;
            return CannotGO;
        }

        //kamene sa posunu DOPRAVA v riadku row
        if (TMProw % 2 == 0 && TMPcol == 1) {
            TEMP = this.BOARD[TMProw][this.n]; // posledny

            for (int i = this.n; i > 1; i--) {

                this.BOARD[TMProw][i] = this.BOARD[TMProw][i - 1];
            }

            this.BOARD[TMProw][TMPcol] = this.FREEFIELD;

            this.FREEFIELD = TEMP;

            for (int k = 0; k < LOG.getPlayers().getCount(); k++) {
                if (LOG.getOnePlayer(k).getX() == TMProw) {
                    int POSUV = LOG.getOnePlayer(k).getY() + 1;
                    if (POSUV > this.n) {
                        POSUV = 1;
                    }

                    LOG.getOnePlayer(k).setXY(TMProw, POSUV);
                }
            }
            CannotGO.width = TMProw;
            CannotGO.height = this.n;
            return CannotGO;
        }

        //kamene sa posunu DOLAVA v riadku row
        if (TMProw % 2 == 0 && TMPcol == this.n) {

            TEMP = this.BOARD[TMProw][1]; // posledny

            for (int i = 1; i < this.n; i++) {
                this.BOARD[TMProw][i] = this.BOARD[TMProw][i + 1];
            }

            this.BOARD[TMProw][TMPcol] = this.FREEFIELD;

            this.FREEFIELD = TEMP;

            for (int k = 0; k < LOG.getPlayers().getCount(); k++) {
                if (LOG.getOnePlayer(k).getX() == TMProw) {
                    int POSUV = LOG.getOnePlayer(k).getY() - 1;
                    if (POSUV < 1) {
                        POSUV = this.n;
                    }

                    LOG.getOnePlayer(k).setXY(TMProw, POSUV);
                }
            }
            CannotGO.width = TMProw;
            CannotGO.height = 1;
            return CannotGO;
        }
        return null;
    }

}
