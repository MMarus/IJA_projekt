package ija.board;

import ija.player.*;
import ija.treasure.*;

/**
 *
 * @author fjezo_000
 */
public class MazeField {

    MazeCard kamen;
    TreasureCard item;

    /**
     *
     */
    public MazeField() {

        this.kamen = null;
        this.item = null;
    }

    /**
     *
     * @return
     */
    public MazeCard getCard() {
        return this.kamen;
    }

    /**
     *
     * @return
     */
    public TreasureCard getTreasure() {
        return this.item;
    }
    
    /**
     *
     */
    public void removeTreasure() {
        this.item = null;
    }

    /**
     *
     * @param c
     */
    public void putCard(MazeCard c) {
        this.kamen = c;
    }

    /**
     *
     * @param kamen
     * @param item
     */
    public void createField(MazeCard kamen, TreasureCard item) {

        this.kamen = kamen;
        this.item = item;
    }

}
