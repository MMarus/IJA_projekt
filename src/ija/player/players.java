/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.player;

import ija.treasure.TreasureCard;

/**
 *
 * @author fjezo_000
 */
public class players {

    int count;
    onePlayer playersField[];
    int onTurn;
    final int MAXPLAY = 4;

    /**
     *
     */
    public players() {
        this.count = 0;
        this.playersField = new onePlayer[MAXPLAY];
        this.onTurn = 0;
    }
    
    /**
     *
     * @param name
     * @param foundCards
     * @param card
     */
    public void createPlayer(String name, int foundCards, TreasureCard card) {
        this.playersField[this.count] = new onePlayer(name, foundCards, card);

        this.count++;
    }

    /**
     *
     */
    public void nextPlayer() {
        this.onTurn++;
        if (this.count == this.onTurn) {
            this.onTurn = 0;
        }
    }

    /**
     *
     * @return
     */
    public int getCount() {
        return count;
    }
    
    public boolean setCount(int num){
        if( num < 0 || num > MAXPLAY)
            return false;
        
        count = num;
        return true;
    }

    /**
     *
     * @return
     */
    public int getOnTurn() {
        return onTurn;
    }
    
    public boolean setOnTurn(int num){
        if( num < 0 || num > MAXPLAY)
            return false;
        
        onTurn = num;
        return true;
    }

    /**
     *
     * @return
     */
    public onePlayer getPlayerOnTurn() {
        return this.playersField[this.onTurn];
    }

    /**
     *
     * @param i
     * @return
     */
    public onePlayer getPlayer(int i) {
        return this.playersField[i];
    }

}
