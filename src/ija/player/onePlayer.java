/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.player;

import ija.treasure.*;

/**
 *
 * @author fjezo_000
 */
public class onePlayer {

    String Name;
    int foundCards;
    TreasureCard myCard;
    int x;
    int y;

    /**
     *
     * @param Name
     * @param foundCards
     * @param myCard
     */
    public onePlayer(String Name, int foundCards, TreasureCard myCard) {
        this.Name = Name;
        this.foundCards = foundCards;
        this.myCard = myCard;

        this.x = 0;
        this.y = 0;
    }

    /**
     *
     * @param x
     * @param y
     */
    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @return
     */
    public int getX() {
        return this.x;
    }

    /**
     *
     * @return
     */
    public int getY() {
        return this.y;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return Name;
    }

    /**
     *
     * @return
     */
    public int getFoundCards() {
        return foundCards;
    }
    
    /**
     * @param i
     */
    public void setFoundCards(int i) {
        this.foundCards = i;
    }
    
    /**
     *
     */
    public void addFoundCards() {
        this.foundCards++;
    }

    /**
     *
     * @return
     */
    public TreasureCard getMyCard() {
        return myCard;
    }

    /**
     *
     * @param myCard
     */
    public void setMyCard(TreasureCard myCard) {
        this.myCard = myCard;
    }

}
