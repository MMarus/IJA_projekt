package ija.treasure;

import java.util.Random;

/**
 *
 * @author fjezo_000
 */
public class CardPack {

    public TreasureCard balicek[];
    int maxSizee;
    int actualSize;

    /**
     *
     * @param maxSize
     * @param initSize
     */
    public CardPack(int maxSize, int initSize) {
        if (maxSize >= initSize) {
            // vytvorime novy balicek kariet s velkostou maxSize
            balicek = new TreasureCard[maxSize];
            this.maxSizee = maxSize;
            // treba vediet kolko kariet je aktualne v balicku
            this.actualSize = initSize;

            // tu sa naplnaju karty do balicku ( na vrchu balicku musi byt karta s pokladom 0 )
            for (int i = 0; i < maxSize; i++) {
                balicek[i] = new TreasureCard(i);
            }
        }
    }

    /**
     * @return Navracia kartu z vrchu balicku musi aj znizit pocet kariet v
     * balicku
     */
    public TreasureCard popCard() {
        return balicek[--this.actualSize];
    }

    /**
     * @return Navracia aktualny pocet kariet v balicku
     */
    public int size() {
        return this.actualSize;
    }

    /**
     * @return Navracia MAXIMALNY POCET KARIET V BALICKU
     */
    public int maxSize() {
        return this.maxSizee;
    }
    
    public boolean setMaxSize(int num){
        if( num != 12 && num != 24)
            return false;
        this.maxSizee = num;
        return true;
    }

    /**
     *
     * @param n
     * @return
     */
    public TreasureCard getTreasure(int n) {
        if (n < 0 || n > maxSizee) {
            return null;
        }
        return balicek[n];
    }
    //
    public boolean setTreasure(int n, int item ) {
        if (n < 0 || n > maxSizee) {
            return false;
        }
        balicek[n].setItem(item);
        return true;
    }

    /**
     *
     */
    public void shuffle() {

        TreasureCard zaloha; // zaloha pre SWAP
        int mojRAND;

        for (int i = 0; i < this.actualSize; i++) {
            // urobi random cislo ale este ho bude treba dat aby to bolo int + rozmedzie
            Random randcislo = new Random();

            mojRAND = randcislo.nextInt(this.actualSize); // nextInt robi rozmedzie random int cisel

            // SWAP
            zaloha = this.balicek[i];
            this.balicek[i] = this.balicek[mojRAND];
            this.balicek[mojRAND] = zaloha;
        }
    }
}
