package ija.treasure;

/**
 *
 * @author fjezo_000
 */
public class TreasureCard {

    int item;

    /**
     *
     * @param item
     */
    public TreasureCard(int item) {
        this.item = item;
    }

    /**
     *
     * @return
     */
    public int getItem() {
        return item;
    }
    public void setItem(int n){
        this.item = item;
    }
}
