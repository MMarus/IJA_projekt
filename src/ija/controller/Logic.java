/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.controller;

import ija.UI.*;
import ija.board.*;
import static ija.controller.GameIO.MSGS;
import ija.player.*;
import ija.treasure.*;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.abs;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fjezo_000
 */
public class Logic {

    private MazeBoard hraciaPlocha;
    private CardPack items;
    private players hraci;
    private Images IMGS;

    int GAMESTATE = 1;
    Dimension LAST = new Dimension(0, 0);
    Dimension CANNOT = new Dimension(0, 0);

    /**
     *
     */
    public GUI OKNO;

    /**
     *
     */
    public Logic() {
        this.hraciaPlocha = null;
        this.items = null;
        this.hraci = null;
        this.IMGS = new Images();

        OKNO = new GUI();
        OKNO.setVisible(true);
    }
    
    public int getGAMESTATE()
    {
        return this.GAMESTATE;
    }

    /**
     *
     * @param SZE
     * @param CRD
     * @param PLY
     * @return
     */
    public int NewGame(int SZE, int CRD, int PLY) {
        // vytvorime balicek kariet + zamiesame
        GAMESTATE = 1;
        items = new CardPack(CRD, CRD);
        items.shuffle();

        // vytvorime hracie pole
        hraciaPlocha = MazeBoard.createMazeBoard(SZE);
        hraciaPlocha.newGame();

        // vytvorime pole hracov, neskor sa bude naplnat
        //plus pridame im X a Y
        this.hraci = new players();

        return 0;
    }

    /**
     *
     * @return
     */
    public onePlayer getPlayer() {
        return hraci.getPlayerOnTurn();
    }

    /**
     *
     * @return
     */
    public players getPlayers() {
        return hraci;
    }

    /**
     *
     * @param i
     * @return
     */
    public onePlayer getOnePlayer(int i) {
        return hraci.getPlayer(i);
    }

    // vlozi hraca
    /**
     *
     * @param name
     */
    public void insertPlayer(String name) {
        //tu bude case atd
        int ID = hraci.getCount();
        int x = 0, y = 0;
        switch (ID) {
            case 0:
                x = 1;
                y = 1;
                break;
            case 1:
                x = 1;
                y = hraciaPlocha.getN();
                break;
            case 2:
                x = hraciaPlocha.getN();
                y = 1;
                break;
            case 3:
                x = hraciaPlocha.getN();
                y = hraciaPlocha.getN();
                break;
        }

        //26 is EMPTY CARD
        TreasureCard CARD = new TreasureCard(26);
        this.hraci.createPlayer(name, 0, CARD);
        this.hraci.getPlayer(ID).setXY(x, y);
    }

    /**
     *
     */
    public void nextTurn() {
        this.hraci.nextPlayer();
    }

    /**
     *
     */
    public void cardClick() {
        onePlayer HRAC = hraci.getPlayerOnTurn();

        if (HRAC.getMyCard().getItem() == 26) {
            System.out.println("HRAC>" + HRAC.getName() + ">>NEMA KARTU");
            TreasureCard item;

            if (items.size() == 0) {
                System.out.println("BALICEK PRAZNY!!!");
                item = new TreasureCard(25);
            } else {
                item = items.popCard();
            }

            // nastavi hracovi tu novu kartu
            HRAC.setMyCard(item);
        }

        //System.out.println("HRAC>" + HRAC.getName() + "MA KARTU>>" + HRAC.getMyCard().getItem());
    }

    /**
     *
     * @return
     */
    public int showMyCard() {
        onePlayer HRAC = hraci.getPlayerOnTurn();

        TreasureCard KARTA = HRAC.getMyCard();

        return KARTA.getItem();

    }

    /**
     *
     * @return CardPack
     */
    public CardPack getItemsPack() {
        return this.items;
    }

    /**
     *
     * @return
     */
    public String showOnTurn() {
        onePlayer HRAC = hraci.getPlayerOnTurn();
        String name = HRAC.getName();

        return name;
    }

    /**
     *
     * @return
     */
    public String showStats() {

        String STATS = "<html><pre><font size=\"4\">\t\tON TURN IS PLAYER: " + hraci.getPlayerOnTurn().getName();

        for (int i = 0; i < hraci.getCount(); i++) {

            STATS += "<br>\t\tName: " + hraci.getPlayer(i).getName() + " ... found items: " + hraci.getPlayer(i).getFoundCards();
        }

        STATS += "</font>";

        String POLE[] = new String[7];
        POLE[0] = "<font size=\"5\" color=\"green\">WIN WIN WIN</font><br>";
        POLE[1] = "GET CARD !<br>";
        POLE[2] = "TURN ROCK AND INSERT ! OR YOU CAN ACCEPT!<br>";
        POLE[3] = "ACCEPT INSERT OR YOU CAN USE UNDO !<br>";
        POLE[4] = "WALK ! THAN ACCEPT !<br>";
        POLE[5] = "<font color=\"green\">GRATULATION ! YOU FOUND YOUR CARD!</font><br>";
        
        STATS += "<br><font size=\"4\" face=\"verdana\" color=\"red\">" + POLE[GAMESTATE] + "</font>";

        return STATS + "</pre></html>";
    }

    /**
     *
     * @return
     */
    public MazeBoard showBoard() {
        return this.hraciaPlocha;
    }

    /**
     *
     * @return
     */
    public Images getIMGS() {
        return this.IMGS;
    }

    ///MAREK EDITED/////
    /**
     * spracovava kliknutia + priebeh hry uchovava krok hry/ pozera sa nan Ak je
     * x, y vecsie ako nula bolo kliknute na BOARD, inak len obyc klik
     */
    public void GameProcess(int Code, int x, int y) {

        //over karrtu ci nevyhravam...
        int plTreas = hraci.getPlayerOnTurn().getMyCard().getItem();
        TreasureCard mfTreas = hraciaPlocha.get(hraci.getPlayerOnTurn().getX(), hraci.getPlayerOnTurn().getY()).getTreasure();

        if (mfTreas != null && plTreas == mfTreas.getItem()) {
            // NASIEL KARTU pripocitame mu ,  a nastavime ze nema kartu!
            hraci.getPlayerOnTurn().addFoundCards();
            hraciaPlocha.get(hraci.getPlayerOnTurn().getX(), hraci.getPlayerOnTurn().getY()).removeTreasure();
            hraci.getPlayerOnTurn().setMyCard(new TreasureCard(26));

            GAMESTATE = 5;

        }

        if ((items.maxSize() / hraci.getCount()) <= hraci.getPlayerOnTurn().getFoundCards()) {
            // HRA KONCI
            GAMESTATE = 0;
        }
        
        switch (GAMESTATE) {
            //1 krok, zober kartu
            case 1:
                if (Code == 11) {
                    cardClick();
                    GAMESTATE = 2;
                }

                break;
            //2 krok, natoc kamen + vloz
            case 2:
                if (Code == 1) {
                    GAMESTATE = 4; // ACCEPT NEVLOZILI SME NIC --> STATE -> 4
                    break;
                }

                if (Code == 12) {
                    hraciaPlocha.getFreeField().getCard().turnRight();
                }

                if (Code == 13) {
                    //System.out.println("CHCEM>x = "+CANNOT.width+" y = "+ CANNOT.height);
                    if (CANNOT.width != x || CANNOT.height != y) {
                        Dimension shifting = hraciaPlocha.shiftFIELD(x, y);
                        if (shifting != null) {
                            CANNOT = shifting;
                            //LAST.width = x;
                            //LAST.height = y;
                            GAMESTATE = 3;
                        }

                    }
                    //System.out.println("CANTGO>x = "+CANNOT.width+" y = "+ CANNOT.height);
                }

                break;

            case 3:
                // prisiel som zo stavu 2 >> MOZEM DAT UNDO BUTTON ALEBO ACCEPT!
                if (Code == 1) {
                    //ACCEPT --> STAV 3
                    GAMESTATE = 4;
                }

                if (Code == 2) {
                    // UNDO BUTTON

                    Dimension shifting = hraciaPlocha.shiftFIELD(CANNOT.width, CANNOT.height);
                    if (shifting != null) {
                        CANNOT = LAST;
                        LAST.width = x;
                        LAST.height = y;
                        GAMESTATE = 2;
                    }
                }
                break;
            
            //4 krok posuv hraca po doske + accept
            case 4:

                if (Code == 13) {
                    posuvHraca(x, y);
                }

                if (Code == 1) {
                    nextTurn();
                    GAMESTATE = 1;
                    if (hraci.getPlayerOnTurn().getMyCard().getItem() != 26) {
                        GAMESTATE = 2;
                    }
                }
                break;

            //NASIEL SOM KARTU!, moze si vypytat dalsiu, a znova na krok 4
            case 5:
                if (Code == 11) {
                    cardClick();
                    GAMESTATE = 4;
                }

                if (Code == 1) {
                    nextTurn();
                    GAMESTATE = 1;
                    if (hraci.getPlayerOnTurn().getMyCard().getItem() != 26) {
                        GAMESTATE = 2;
                    }
                }
                break;

            //KONIEC HRY -- NIEKTO VYHRAL!!
            case 0:
                break;
        }

    }

    public void posuvHraca(int x, int y) {
        // ak je policko o 1 vzdialene mozeme tam hraca posunut
        //zisti o kolko sa posuvame
        onePlayer player = getPlayer();
        //Ideme len o jedno ??? 
        int shiftCount = abs(player.getX() - x) + abs(player.getY() - y);

        //Mozem ist do daneho smeru ?? 
        // Idem na druhu straunu 
        
        int playerX = player.getX();
        int playerY = player.getY();

        int horizontal = player.getY() - y;
        int vertical = player.getX() - x;
        
        if( abs(player.getX() - x) == (hraciaPlocha.getN()-1) && abs(player.getY() - y) == 0)
            shiftCount = 1;
        if( abs(player.getY() - y) == (hraciaPlocha.getN()-1) && abs(player.getX() - x) == 0)
            shiftCount = 1;
        

        //Mozem ist Do prava??
        if (horizontal == -1 || horizontal == (hraciaPlocha.getN()-1)  )  {
            //System.out.println("chcem ist do prava?? player " + (getPlayer().getX() - x) + ""            );
            //Ak nemozeme doprava, return
            if (!hraciaPlocha.get(player.getX(), player.getY()).getCard().canGo(MazeCard.CANGO.RIGHT)) {
                return;
            }
            if (!hraciaPlocha.get(x, y).getCard().canGo(MazeCard.CANGO.LEFT)) {
                return;
            }
        }
        //Mozem ist Do lava??
        if (horizontal == 1 || horizontal == (hraciaPlocha.getN()-1)*(-1)) {
            if (!hraciaPlocha.get(player.getX(), player.getY()).getCard().canGo(MazeCard.CANGO.LEFT)) {
                return;
            }
            if (!hraciaPlocha.get(x, y).getCard().canGo(MazeCard.CANGO.RIGHT)) {
                return;
            }
        }
        //Mozem ist Dole??
        if (vertical == -1 || vertical == (hraciaPlocha.getN()-1) ) {
            if (!hraciaPlocha.get(player.getX(), player.getY()).getCard().canGo(MazeCard.CANGO.DOWN)) {
                return;
            }
            if (!hraciaPlocha.get(x, y).getCard().canGo(MazeCard.CANGO.UP)) {
                return;
            }
        }
        //Mozem ist Hore??
        if (vertical == 1 || vertical == (hraciaPlocha.getN()-1)*(-1)) {
            if (!hraciaPlocha.get(player.getX(), player.getY()).getCard().canGo(MazeCard.CANGO.UP)) {
                return;
            }
            if (!hraciaPlocha.get(x, y).getCard().canGo(MazeCard.CANGO.DOWN)) {
                return;
            }
        }

        if (shiftCount == 1) {
            //posuneme
            getPlayer().setXY(x, y);
        }

    }

    public ArrayList<String> getSavedGames() {
        ArrayList<String> games = new ArrayList<>();

        File folder = new File("savedGames");
        File[] listOfFiles = folder.listFiles();
        games.add("Zvolte hru");

        if (folder.exists()) {
            for (File listOfFile : listOfFiles) {
                if (listOfFile.isFile()) {
                    System.out.println("File " + listOfFile.getName());
                    games.add(listOfFile.getName());
                } else if (listOfFile.isDirectory()) {
                    System.out.println("Directory " + listOfFile.getName());
                }
            }
        } else {
            System.out.println("Priecinok neexistuje");
        }

        return games;
    }

    /**
     *
     * @param GameName
     * @return
     */
    public int LoadGame(String GameName) {

        File file = new File("savedGames/" + GameName);
        
        if (! file.exists() || ! file.canRead() ){
            return 1;
        }
        
        int NEW_GAMESTATE;
        Dimension NEW_LAST;
        Dimension NEW_CANNOT;
        int NEW_PlayerCount;
        MazeBoard NEW_hraciaPlocha;
        CardPack NEW_items;
        players NEW_hraci;
        
        
        //Najskor si nacitaj subor
        int data, i;
        try (BufferedReader br = new BufferedReader(new FileReader("savedGames/" + GameName))) {

            //###### MEGA MEGA SUPER LABIRINT SAVE ######
            if (!MSGS[0].equals(br.readLine() + "\n")) {
                return 1;
            }

            //### GAMEINFO
            if (!MSGS[1].equals(br.readLine() + "\n")) {
                return 1;
            }

            //GameState
            data = Integer.parseInt(br.readLine());
            if (data < 1 || data > 5) {
                return 1;
            }
            NEW_GAMESTATE = data;

            //LAST
            NEW_LAST = new Dimension(Integer.parseInt(br.readLine()), Integer.parseInt(br.readLine()));
            
            //CANNOT
            NEW_CANNOT = new Dimension(Integer.parseInt(br.readLine()), Integer.parseInt(br.readLine()));;

            //### ALL PLAYERS
            if (!MSGS[2].equals(br.readLine() + "\n")) {
                return 1;
            }
            //Pocet palyerov
            NEW_PlayerCount = Integer.parseInt(br.readLine());
            if (NEW_PlayerCount < 1 || NEW_PlayerCount > 4) {
                return 1;
            }

            //vytvorime novych hracov
            NEW_hraci = new players();

            int onTurn = Integer.parseInt(br.readLine());
            //On turn - ak je onTurn vecsi ako pocet hracov, vrat chybu.
            if (NEW_PlayerCount < onTurn || false == NEW_hraci.setOnTurn(onTurn)) {
                return 1;
            }

            //Players - po jednom nacitame
            for (i = 0; i < data; i++) {

                String l = br.readLine();
                String m = MSGS[3] + (i + 1);
                if (!l.equals(m)) {
                    return 1;
                }

                String name = br.readLine();
                int foundCards = Integer.parseInt(br.readLine());
                int playX = Integer.parseInt(br.readLine());
                int playY = Integer.parseInt(br.readLine());
                int ItemNum = Integer.parseInt(br.readLine());

                NEW_hraci.createPlayer(name, foundCards, new TreasureCard(ItemNum));
                NEW_hraci.getPlayer(i).setXY(playX, playY);
                System.out.println("Player" + i + " added");
            }

            if (!MSGS[4].equals(br.readLine() + "\n")) {
                return 1;
            }

            //CARD PACK
            int maxSize = Integer.parseInt(br.readLine());
            if (maxSize != 12 && maxSize != 24) {
                return 1;
            }
            NEW_items = new CardPack(maxSize, Integer.parseInt(br.readLine()));

            //Po jednom vlozime itemy
            for (i = 0; i < maxSize; i++) {
                String l = br.readLine();
                String m = MSGS[5] + (i + 1);
                if (!l.equals(m)) {
                    return 1;
                }
                int item = Integer.parseInt(br.readLine());
                
                NEW_items.balicek[i] = new TreasureCard(item);
                System.out.println("item" + i + " added");
            }

            if (!MSGS[6].equals(br.readLine() + "\n")) {
                return 1;
            }

            int allBoard = Integer.parseInt(br.readLine());
            if (allBoard < 5 || allBoard > 11) {
                return 1;
            }
            NEW_hraciaPlocha = MazeBoard.createMazeBoard(allBoard);

            //FREEFIELD
            if (!MSGS[7].equals(br.readLine() + "\n")) {
                return 1;
            }

            int item = Integer.parseInt(br.readLine());
            int down = Integer.parseInt(br.readLine());
            int left = Integer.parseInt(br.readLine());
            int right = Integer.parseInt(br.readLine());
            int up = Integer.parseInt(br.readLine());

            if (! NEW_hraciaPlocha.setFreeField(item, down, left, right, up)) {
                return 1;
            }

            for (i = 1; i <= allBoard; i++) {
                for (int j = 1; j <= allBoard; j++) {
                    //FIELDS on board
                    String l = br.readLine();
                    String m = MSGS[8] + (i) + MSGS[9] + (j);
                    if (!l.equals(m)) {
                        return 1;
                    }

                    item = Integer.parseInt(br.readLine());
                    down = Integer.parseInt(br.readLine());
                    left = Integer.parseInt(br.readLine());
                    right = Integer.parseInt(br.readLine());
                    up = Integer.parseInt(br.readLine());

                    if (! NEW_hraciaPlocha.setField(i, j, item, down, left, right, up)) {
                        return 1;
                    }
                    System.out.println("FIELD x=" + (i + 1) + " y=" + (j + 1) + " added");
                }
            }
            if (!MSGS[10].equals(br.readLine() + "\n")) {
                return 1;
            }
            //ak last je vecsie alebo cannot vecsie ako velkost hracej plochy return 1
            if( NEW_LAST.width > allBoard || NEW_LAST.height > allBoard)
                return 1;
            if(NEW_CANNOT.width > allBoard || NEW_CANNOT.height > allBoard)
                return 1;
            
            //Ak vsetko preslo v pohode tak mozme prepisat aktualnu hru
            hraciaPlocha = NEW_hraciaPlocha;
            items = NEW_items;
            hraci = NEW_hraci;
           
            GAMESTATE = NEW_GAMESTATE;
            LAST = NEW_LAST;
            CANNOT = NEW_CANNOT;
            
            /*
            int NEW_GAMESTATE;
            Dimension NEW_LAST;
            Dimension NEW_CANNOT;
            int NEW_PlayerCount;
            MazeBoard NEW_hraciaPlocha;
            CardPack NEW_items;
            players NEW_hraci;
            */

        } catch (FileNotFoundException ex) {
            //Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        } catch (IOException ex) {
            //Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }

        return 0;
    }
    //Cheaty 
    public void Cheats(int n){
        
        //GODMODE
        if( n == 1){
            int item;
            for( int x = 1; x <= hraciaPlocha.getN(); x++){
                for( int y = 1; y <= hraciaPlocha.getN(); y++){
                    if(hraciaPlocha.get(x, y).getTreasure() == null )
                        item = -1;
                    else 
                        item = hraciaPlocha.get(x, y).getTreasure().getItem();
                    hraciaPlocha.setField(x, y, item, 1, 1, 1, 1);
                }
            }
        
        }
    }
}
