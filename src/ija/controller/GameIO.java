/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.controller;

import static ija.ija.LOG;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xjezov01
 */
public class GameIO {
    public static final String[] MSGS = {
        "###### MEGA MEGA SUPER LABIRINT SAVE ######\n", 
        "### GAMEINFO\n",
        "### ALL PLAYERS\n",
        "#PLAYER ",
        "### ALL ITEMS\n",
        "#ITEM",
        "### ALL BOARD\n",
        "##FREEFIELD\n",
        "#FIELD >> X:",
        ">> Y:",        
        "###### END OF SAVE ######\n"    
    };
    
    
    public static int save(String GAMENAME, boolean ULOZ) {
        String DATA = MSGS[0];

        DATA += saveInfo();
        DATA += savePlayer();
        DATA += saveItems();
        DATA += saveBoard();

        DATA += MSGS[10];

        return write(DATA, GAMENAME, ULOZ);
    }

    public static String saveInfo() {
        String DATA = MSGS[1];

        //GAMESTATE
        DATA += LOG.GAMESTATE + "\n";
        // LAST
        DATA += LOG.LAST.width + "\n";
        DATA += LOG.LAST.height + "\n";
        //CANNOT
        DATA += LOG.CANNOT.width + "\n";
        DATA += LOG.CANNOT.height + "\n";

        return DATA;
    }

    public static String savePlayer() {
        String DATA = MSGS[2];
        // COUNT + ONTURN
        DATA += LOG.getPlayers().getCount() + "\n";
        DATA += LOG.getPlayers().getOnTurn() + "\n";

        //ONEPLAYER
        for (int i = 0; i < LOG.getPlayers().getCount(); i++) {
            DATA += MSGS[3] + (i + 1) + "\n";
            DATA += LOG.getOnePlayer(i).getName() + "\n";
            DATA += LOG.getOnePlayer(i).getFoundCards() + "\n";
            DATA += LOG.getOnePlayer(i).getX() + "\n";
            DATA += LOG.getOnePlayer(i).getY() + "\n";
            DATA += LOG.getOnePlayer(i).getMyCard().getItem() + "\n";
        }

        return DATA;

    }

    public static String saveItems() {
        String DATA = MSGS[4];
        //MAXSIZE
        DATA += LOG.getItemsPack().maxSize() + "\n";
        //ACTUAL SIZE
        DATA += LOG.getItemsPack().size() + "\n";

        for (int i = 0; i < LOG.getItemsPack().maxSize(); i++) {
            DATA += MSGS[5] + (i + 1) + "\n";
            DATA += LOG.getItemsPack().getTreasure(i).getItem() + "\n";
        }

        return DATA;
    }

    public static String saveBoard() {
        String DATA = MSGS[6];
        // GAME N x N
        DATA += LOG.showBoard().getN() + "\n";

        //FREEFIELD
        DATA += MSGS[7];
        //ITEM OF FREEFIELD
        if (LOG.showBoard().getFreeField().getTreasure() == null) {
            DATA += "-1\n";
        } else {
            DATA += LOG.showBoard().getFreeField().getTreasure().getItem() + "\n";
        }

        //ROCK DOWN
        if (LOG.showBoard().getFreeField().getCard().down != null) {
            DATA += "1\n";
        } else {
            DATA += "0\n";
        }
        //ROCK LEFT
        if (LOG.showBoard().getFreeField().getCard().left != null) {
            DATA += "1\n";
        } else {
            DATA += "0\n";
        }
        //ROCK RIGHT
        if (LOG.showBoard().getFreeField().getCard().right != null) {
            DATA += "1\n";
        } else {
            DATA += "0\n";
        }
        //ROCK UP
        if (LOG.showBoard().getFreeField().getCard().up != null) {
            DATA += "1\n";
        } else {
            DATA += "0\n";
        }

        // ALL FIELDS ON BOARD
        for (int x = 1; x <= LOG.showBoard().getN(); x++) {
            for (int y = 1; y <= LOG.showBoard().getN(); y++) {
                DATA += MSGS[8] + (x) + MSGS[9] + (y) + "\n";

                //ITEM
                if (LOG.showBoard().get(x, y).getTreasure() == null) {
                    DATA += "-1\n";
                } else {
                    DATA += LOG.showBoard().get(x, y).getTreasure().getItem() + "\n";
                }

                //ROCK DOWN
                if (LOG.showBoard().get(x, y).getCard().down != null) {
                    DATA += "1\n";
                } else {
                    DATA += "0\n";
                }
                //ROCK LEFT
                if (LOG.showBoard().get(x, y).getCard().left != null) {
                    DATA += "1\n";
                } else {
                    DATA += "0\n";
                }
                //ROCK RIGHT
                if (LOG.showBoard().get(x, y).getCard().right != null) {
                    DATA += "1\n";
                } else {
                    DATA += "0\n";
                }
                //ROCK UP
                if (LOG.showBoard().get(x, y).getCard().up != null) {
                    DATA += "1\n";
                } else {
                    DATA += "0\n";
                }

            }
        }

        return DATA;
    }

    public static int write(String DATA, String GAMENAME, boolean ULOZ) {

        Writer writer = null;
        if ( "".equals(GAMENAME) )
        {
            java.util.Date date= new java.util.Date();
            GAMENAME = (date.getYear()+1900) + "-" + (date.getMonth()+1) + "-" + date.getDate() + "---";
            GAMENAME += date.getHours() + "-" + date.getMinutes() + "-" + date.getSeconds();
        }

        //CREATE FOLDER FOR SAVE
        File folder = new File("savedGames");
        if (!folder.exists()) {
            //dont exist , i create
            if (folder.mkdir() == false) {
                return 10; // NEDA SA
            }
        }

        if (ULOZ == false) {
            File f = new File("savedGames/" + GAMENAME);
            if (f.exists()) {
                //existuje
                return 5;
            }

        }

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("savedGames/" + GAMENAME), "utf-8"));
            writer.write(DATA);
        } catch (IOException ex) {
            return 10;
        }
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(GameIO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }

}
